package com.cartaoms.cliente.service;


import com.cartaoms.cliente.exception.ClientNotFoundException;
import com.cartaoms.cliente.model.Client;
import com.cartaoms.cliente.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClientService {

    @Autowired
    private ClientRepository clientRepository;

    public Client create(Client client) {
        return clientRepository.save(client);
    }

    public Client getById(Long id) {
        Optional<Client> byId = clientRepository.findById(id);

        if(!byId.isPresent()) {
            throw new ClientNotFoundException();
        }

        return byId.get();
    }

}
