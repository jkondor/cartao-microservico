package com.cartaoms.cliente.repository;


import com.cartaoms.cliente.model.Client;
import org.springframework.data.repository.CrudRepository;

public interface ClientRepository extends CrudRepository<Client, Long> {

}
