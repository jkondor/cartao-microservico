package com.cartaoms.pagamento.cartao;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

@FeignClient(name = "cartao", configuration = ClientCartaoConfiguration.class)
public interface ClienteCartao {

    @GetMapping("/cartao/id/{id}")
    ClienteCartaoDTO getById(@PathVariable Long id);

    }
