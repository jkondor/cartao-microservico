package com.cartaoms.pagamento.cartao;

import feign.RetryableException;

import java.io.IOException;

public class ClienteCartaoFallback implements ClienteCartao{

    private Exception cause;

    ClienteCartaoFallback(Exception cause) {
        this.cause = cause;
    }

    @Override
    public ClienteCartaoDTO getById(Long id) {
        System.out.println("create CAUSA = " + cause.getCause());
        System.out.println("create CLASSE = " + cause.getClass());

        if(cause instanceof IOException || cause instanceof RetryableException || cause.getLocalizedMessage().contains("ConnectException")) {
            throw new RuntimeException("O serviço de carro está offline");
        }else {
            throw (RuntimeException) cause;
        }
    }
}
