package com.cartaoms.pagamento.cartao;

import feign.Feign;
import feign.codec.ErrorDecoder;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;
import org.springframework.context.annotation.Bean;

public class ClientCartaoConfiguration {
    @Bean
    public ErrorDecoder getErrorDecoder() {
        return new ClienteCartaoErrorDecoder();
    }

    @Bean
    public Feign.Builder builder() {
        FeignDecorators decorators = FeignDecorators.builder()
                .withFallbackFactory(ClienteCartaoFallback::new)
                .build();

        return Resilience4jFeign.builder(decorators);
    }
}
