package com.cartaoms.pagamento.controller;

import com.cartaoms.pagamento.model.Pagamento;
import com.cartaoms.pagamento.model.dto.CreatePagamentoRequest;
import com.cartaoms.pagamento.model.dto.PagamentoResponse;
import com.cartaoms.pagamento.model.mapper.PagamentoMapper;
import com.cartaoms.pagamento.service.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class PagamentoController {

    @Autowired
    private PagamentoService pagamentoService;

    @PostMapping("/pagamento")
    public PagamentoResponse create(@RequestBody CreatePagamentoRequest createPagamentoRequest) {

        Pagamento pagamento = PagamentoMapper.toPagamento(createPagamentoRequest);

        pagamento = pagamentoService.create(pagamento);

        return PagamentoMapper.toPagamentoResponse(pagamento);
    }

    @GetMapping("/pagamentos/{cartaoId}")
    public List<PagamentoResponse> listByCartao(@PathVariable Long cartaoId) {
        List<Pagamento> pagamentos = pagamentoService.listByCartao(cartaoId);
        return PagamentoMapper.toPagamentoResponse(pagamentos);
    }

}
