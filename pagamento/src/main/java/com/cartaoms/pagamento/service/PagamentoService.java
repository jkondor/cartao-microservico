package com.cartaoms.pagamento.service;


import com.cartaoms.pagamento.cartao.ClienteCartao;
import com.cartaoms.pagamento.cartao.ClienteCartaoDTO;
import com.cartaoms.pagamento.exception.CartaoInativeException;
import com.cartaoms.pagamento.exception.CartaoNotFoundException;
import com.cartaoms.pagamento.model.Pagamento;
import com.cartaoms.pagamento.repository.PagamentoRepository;
import feign.FeignException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PagamentoService {

    @Autowired
    private PagamentoRepository pagamentoRepository;

    @Autowired
    private ClienteCartao clienteCartaoService;

    public Pagamento create(Pagamento pagamento) {
        try {

            System.out.println("CARTAOID = " + pagamento.getCartaoId());

            ClienteCartaoDTO clienteCartaoDTO = clienteCartaoService.getById(pagamento.getCartaoId());

            System.out.println("CARTAODTOID = " + clienteCartaoDTO.getId());
            System.out.println("CARTAODTOATIVO = " + clienteCartaoDTO.getAtivo());

            if (clienteCartaoDTO.getAtivo()==false){
                throw new CartaoInativeException();
            }

            return pagamentoRepository.save(pagamento);
        }
        catch (FeignException.NotFound e) {
            throw new CartaoNotFoundException();
        }
    }

    public List<Pagamento> listByCartao(Long cartaoId) {
        return pagamentoRepository.findAllByCartaoId(cartaoId);
    }

}
