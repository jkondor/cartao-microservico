package com.cartaoms.pagamento.repository;


import com.cartaoms.pagamento.model.Pagamento;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PagamentoRepository extends CrudRepository<Pagamento, Long> {

    List<Pagamento> findAllByCartaoId(Long cartaoId);

}
