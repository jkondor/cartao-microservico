package com.cartaoms.cartao.controller;


import com.cartaoms.cartao.model.Cartao;
import com.cartaoms.cartao.model.dto.create.CreateCartaoRequest;
import com.cartaoms.cartao.model.dto.create.CreateCartaoResponse;
import com.cartaoms.cartao.model.dto.get.GetCartaoResponse;
import com.cartaoms.cartao.model.dto.update.UpdateCartaoRequest;
import com.cartaoms.cartao.model.dto.update.UpdateCartaoResponse;
import com.cartaoms.cartao.model.mapper.CartaoMapper;
import com.cartaoms.cartao.service.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/cartao")
public class CartaoController {

    @Autowired
    private CartaoService cartaoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CreateCartaoResponse create(@RequestBody CreateCartaoRequest createCartaoRequest) {

        Cartao cartao = CartaoMapper.fromCreateRequest(createCartaoRequest);

        cartao = cartaoService.create(cartao);

        return CartaoMapper.toCreateResponse(cartao);
    }

    @PatchMapping("/{numero}")
    public UpdateCartaoResponse update(@PathVariable String numero, @RequestBody UpdateCartaoRequest updateCartaoRequest) {

        Cartao cartao = CartaoMapper.fromUpdateRequest(updateCartaoRequest);
        cartao.setNumero(numero);

        cartao = cartaoService.update(cartao);
        return CartaoMapper.toUpdateResponse(cartao);
    }

    @GetMapping("/{numero}")
    public GetCartaoResponse getByNumero(@PathVariable String numero) {
        Cartao byNumero = cartaoService.getByNumero(numero);

        return CartaoMapper.toGetResponse(byNumero);
    }

    @GetMapping("/id/{id}")
    public UpdateCartaoResponse getById(@PathVariable Long id) {
        Cartao byId = cartaoService.getById(id);

        return CartaoMapper.toUpdateResponse(byId);
    }


}
