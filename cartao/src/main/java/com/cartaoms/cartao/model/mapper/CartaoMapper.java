package com.cartaoms.cartao.model.mapper;


import com.cartaoms.cartao.cliente.ClientDTO;
import com.cartaoms.cartao.model.Cartao;
import com.cartaoms.cartao.model.dto.create.CreateCartaoRequest;
import com.cartaoms.cartao.model.dto.create.CreateCartaoResponse;
import com.cartaoms.cartao.model.dto.get.GetCartaoResponse;
import com.cartaoms.cartao.model.dto.update.UpdateCartaoRequest;
import com.cartaoms.cartao.model.dto.update.UpdateCartaoResponse;

public class CartaoMapper {

    public static Cartao fromCreateRequest(CreateCartaoRequest cartaoCreateRequest) {

        Cartao cartao = new Cartao();
        cartao.setNumero(cartaoCreateRequest.getNumero());
        cartao.setClientId(cartaoCreateRequest.getClienteId());
        return cartao;
    }

    public static CreateCartaoResponse toCreateResponse(Cartao cartao) {
        CreateCartaoResponse createCartaoResponse = new CreateCartaoResponse();

        createCartaoResponse.setId(cartao.getId());
        createCartaoResponse.setNumero(cartao.getNumero());
        createCartaoResponse.setClienteId(cartao.getClientId());
        createCartaoResponse.setAtivo(cartao.getAtivo());

        return createCartaoResponse;
    }

    public static Cartao fromUpdateRequest(UpdateCartaoRequest updateCartaoRequest) {
        Cartao cartao = new Cartao();
        cartao.setAtivo(updateCartaoRequest.getAtivo());
        return cartao;
    }

    public static UpdateCartaoResponse toUpdateResponse(Cartao cartao) {
        UpdateCartaoResponse updateCartaoResponse = new UpdateCartaoResponse();

        updateCartaoResponse.setId(cartao.getId());
        updateCartaoResponse.setNumero(cartao.getNumero());
        updateCartaoResponse.setClienteId(cartao.getClientId());
        updateCartaoResponse.setAtivo(cartao.getAtivo());

        return updateCartaoResponse;
    }

    public static GetCartaoResponse toGetResponse(Cartao cartao) {
        GetCartaoResponse getCartaoResponse = new GetCartaoResponse();

        getCartaoResponse.setId(cartao.getId());
        getCartaoResponse.setNumero(cartao.getNumero());
        getCartaoResponse.setClienteId(cartao.getClientId());

        return getCartaoResponse;
    }

}
