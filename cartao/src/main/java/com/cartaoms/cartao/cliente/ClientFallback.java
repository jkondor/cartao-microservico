package com.cartaoms.cartao.cliente;

import com.cartaoms.cartao.exception.ClientNotFoundException;
import feign.RetryableException;

import java.io.IOException;
import java.net.ConnectException;

public class ClientFallback implements Client {

    private Exception cause;

    ClientFallback(Exception cause) {
        this.cause = cause;
    }

    @Override
    public ClientDTO create(Client client){
        System.out.println("create CAUSA = " + cause.getCause());
        System.out.println("create CLASSE = " + cause.getClass());

        if(cause instanceof IOException|| cause instanceof RetryableException || cause.getLocalizedMessage().contains("ConnectException")) {
            throw new RuntimeException("O serviço de carro está offline");
        }else {
            throw (RuntimeException) cause;
        }
    }

    @Override
    public ClientDTO getById(Long id) {

        System.out.println("CAUSA = " + cause.getCause());
        System.out.println("CLASSE = " + cause.getClass());
        if(cause instanceof IOException) {
            throw new RuntimeException("O serviço de carro está offline");
        }else {
            throw (RuntimeException) cause;
            }
    }
}
