package com.cartaoms.cartao.cliente;

import org.springframework.cloud.openfeign.FeignClient;

import org.springframework.web.bind.annotation.*;

@FeignClient(name = "cliente", configuration = ClientConfiguration.class)
public interface Client {

    @PostMapping("/cliente")
    ClientDTO create(@RequestBody Client client);

    @GetMapping("/cliente/{id}")
    ClientDTO getById(@PathVariable Long id);

}
