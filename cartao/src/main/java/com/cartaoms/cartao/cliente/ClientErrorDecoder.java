package com.cartaoms.cartao.cliente;

import com.cartaoms.cartao.exception.ClientNotFoundException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class ClientErrorDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if (response.status()==404){
            return new ClientNotFoundException();
        }else{
            return errorDecoder.decode(s, response);
        }
    }
}
