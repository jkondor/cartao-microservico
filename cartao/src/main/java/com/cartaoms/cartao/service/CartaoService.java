package com.cartaoms.cartao.service;


import com.cartaoms.cartao.cliente.Client;
import com.cartaoms.cartao.cliente.ClientDTO;
import com.cartaoms.cartao.exception.CartaoAlreadyExistsException;
import com.cartaoms.cartao.exception.CartaoNotFoundException;
import com.cartaoms.cartao.exception.ClientNotFoundException;
import com.cartaoms.cartao.model.Cartao;
import com.cartaoms.cartao.repository.CartaoRepository;
import feign.FeignException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    private CartaoRepository cartaoRepository;

    @Autowired
    private Client clientService;

    public Cartao create(Cartao cartao) {

            ClientDTO clientbyId = clientService.getById(cartao.getClientId());
        System.out.println("Continuou mesmo com cliente inexistente. cliente = " + clientbyId.getName());
            Optional<Cartao> byNumero = cartaoRepository.findByNumero(cartao.getNumero());

            if(byNumero.isPresent()) {
                throw new CartaoAlreadyExistsException();
            }

            // Regras de negócio
            cartao.setAtivo(false);

            return cartaoRepository.save(cartao);
    }

    public Cartao update(Cartao updatedCartao) {
        Cartao databaseCartao = getByNumero(updatedCartao.getNumero());

        databaseCartao.setAtivo(updatedCartao.getAtivo());

        return cartaoRepository.save(databaseCartao);
    }

    public Cartao getById(Long id) {
        Optional<Cartao> byId = cartaoRepository.findById(id);

        if(!byId.isPresent()) {
            throw new CartaoNotFoundException();
        }

        return byId.get();
    }

    public Cartao getByNumero(String numero) {
        Optional<Cartao> byId = cartaoRepository.findByNumero(numero);

        if(!byId.isPresent()) {
            throw new CartaoNotFoundException();
        }

        return byId.get();
    }

}
